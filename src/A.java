public class A extends AbstractSay {
    @Override
    public void saying(String word) {
        System.out.println("A.Hello " + word);
    }

    @Override
    public void doing(String sth) {
        System.out.println("A.Playing " + sth);
    }
}
