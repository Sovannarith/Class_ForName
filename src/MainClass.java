public class MainClass {


    public static void main(String[] args) {
        try {
            Class clazz = Class.forName("B");
            AbstractSay say = (AbstractSay) clazz.newInstance();

            say.doing("Tennis");
            say.saying("World");


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
