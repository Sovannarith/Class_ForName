public class B extends AbstractSay {
    @Override
    public void saying(String word) {
        System.out.println("B.Hello " + word);
    }

    @Override
    public void doing(String sth) {
        System.out.println("B.Playing " + sth);
    }
}
